# Facets Content type or Other

## CONTENTS OF THIS FILE

- Requirements
- Installation
- Configuration
- Maintainers

### INTRODUCTION

The Facets Content type or Other module provides a way to index some content
types as "first-order" content types, and all other content types are indexed
as "Other". The content type labels can be overridden in the configuration.

### REQUIREMENTS

- Facets - https://www.drupal.org/project/facets
- Search API - https://www.drupal.org/project/search_api

### INSTALLATION

- Install as you would normally install a contributed Drupal module. Visit:
  https://www.drupal.org/node/1897420 for further information.

### CONFIGURATION

1. Configure the Content type or Other settings at
   `/admin/config/search/facets-content-type-or-other`.
2. In your Search index, go to Fields and add a "Content type or other" field.
3. Go to Facets and add a Facet for Content type or other.
4. Under "Facet sorting", select "Content type or Other - Sort order" and make
   sure that all other sorting options are deselected.
5. Re-index all content.

If more complex logic is required to determine the label, you can subscribe to
the event `SetIndexedValue`. See: https://www.drupal.org/docs/creating-modules/subscribe-to-and-dispatch-events

Example event subscriber:
```
class AlterIndexedValueSubscriber implements EventSubscriberInterface {

  public static function getSubscribedEvents() {
    return [
      SetIndexedValue::EVENT_NAME => 'onSetIndexedValue',
    ];
  }

  public function onSetIndexedValue(SetIndexedValue $event) {
    $entity = $event->getEntity();
    $value = $event->getValue();

    if ($entity->bundle() == 'page') {
      $value = 'Page';
    }

    $event->setValue($value);
  }

}
```

### MAINTAINERS

- Wes Jones (earthday47) - https://www.drupal.org/u/earthday47
