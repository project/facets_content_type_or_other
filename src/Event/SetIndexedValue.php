<?php

namespace Drupal\facets_content_type_or_other\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Entity\EntityInterface;

/**
 * Event fired right before the value is set during indexing.
 */
class SetIndexedValue extends Event {

  const EVENT_NAME = 'facets_content_type_or_other_set_indexed_value';

  /**
   * The node entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  public $entity;

  /**
   * The value.
   *
   * @var string
   */
  protected $value;

  /**
   * Constructs the object.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The node entity being indexed.
   * @param string $value
   *   The value.
   */
  public function __construct(EntityInterface $entity, string $value) {
    $this->entity = $entity;
    $this->value = $value;
  }

  /**
   * Retrieves the node entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The node entity being indexed.
   */
  public function getEntity(): EntityInterface {
    return $this->entity;
  }

  /**
   * Retrieves the value.
   *
   * @param string $value
   *   The value.
   */
  public function getValue(): string {
    return $this->value;
  }

  /**
   * Sets the value.
   *
   * @param string $value
   *   The new value.
   */
  public function setValue(string $value) {
    $this->value = $value;
  }

}
