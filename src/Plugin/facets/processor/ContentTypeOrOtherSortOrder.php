<?php

namespace Drupal\facets_content_type_or_other\Plugin\facets\processor;

use Drupal\facets\Processor\SortProcessorInterface;
use Drupal\facets\Processor\SortProcessorPluginBase;
use Drupal\facets\Result\Result;

/**
 * Provides a processor that hides results that don't narrow results.
 *
 * @FacetsProcessor(
 *   id = "content_type_or_other_sort",
 *   label = @Translation("Content type or Other - Sort order"),
 *   description = @Translation("Sort the results of Content type or Other so 'Other' is always the last item. NOTE: The value of 'Sort order' radio input will be ignored."),
 *   stages = {
 *     "sort" = 10
 *   }
 * )
 */
class ContentTypeOrOtherSortOrder extends SortProcessorPluginBase implements SortProcessorInterface {

  protected $firstOrderContentTypes;

  /**
   * Gets the ordering of first-order content types that go before "Other".
   *
   * @return array
   *   Custom sort order.
   */
  protected function getFirstOrderContentTypesSortOrder() {
    if (!$this->firstOrderContentTypes) {
      $first_order_types = [];

      $config = \Drupal::config('facets_content_type_or_other.settings')->get('first_order_config');
      foreach ($config as $key => $row) {
        if ($row['first_order']) {
          $first_order_types[] = $row['label_override'];
        }
      }

      $this->firstOrderContentTypes = $first_order_types;
    }

    return array_merge($this->firstOrderContentTypes, ['Other']);
  }

  /**
   * {@inheritdoc}
   */
  public function sortResults(Result $a, Result $b) {
    $first_order_sort_config = $this->getFirstOrderContentTypesSortOrder();

    $a_pos = array_search($a->getRawValue(), $first_order_sort_config);
    $b_pos = array_search($b->getRawValue(), $first_order_sort_config);

    return ($a_pos <=> $b_pos);
  }

}
