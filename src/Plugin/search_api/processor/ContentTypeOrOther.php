<?php

namespace Drupal\facets_content_type_or_other\Plugin\search_api\processor;

use Drupal\facets_content_type_or_other\Event\SetIndexedValue;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Processor\ProcessorProperty;
use Drupal\search_api\Datasource\DatasourceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Provides a processor for content type or other.
 *
 * @SearchApiProcessor(
 *   id = "content_type_or_other",
 *   label = @Translation("Content type or Other"),
 *   description = @Translation("Displays content types by label and and Other category."),
 *   stages = {
 *     "add_properties" = 0,
 *   },
 *   locked = true,
 *   hidden = true,
 * )
 */
class ContentTypeOrOther extends ProcessorPluginBase {

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  protected $firstOrderContentTypes;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var static $processor */
    $processor = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    $processor->eventDispatcher = $container->get('event_dispatcher');

    return $processor;
  }

  /**
   * Retrieves the event dispatcher.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The event dispatcher.
   */
  public function getEventDispatcher() {
    return $this->eventDispatcher ?: \Drupal::service('event_dispatcher');
  }


  /**
   * Returns the first-order content types not to be indexed as 'Other'.
   */
  public function getFirstOrderContentTypes() {
    if (!$this->firstOrderContentTypes) {
      $config = \Drupal::config('facets_content_type_or_other.settings');
      $first_order_content_types = [];
      foreach ($config->get('first_order_config') as $key => $row) {
        if ($row['first_order']) {
          $first_order_content_types[$key] = $row['label_override'];
        }
      }
      $this->firstOrderContentTypes = $first_order_content_types;
    }
    return $this->firstOrderContentTypes;
  }

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions(DatasourceInterface $datasource = NULL) {
    $properties = [];

    if (!$datasource) {
      $definition = [
        'label' => $this->t('Content type or other'),
        'description' => $this->t('Marks specified content type as Other'),
        'type' => 'string',
        'processor_id' => $this->getPluginId(),
      ];
      $properties['content_type_or_other'] = new ProcessorProperty($definition);
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function addFieldValues(ItemInterface $item) {
    $first_order_types = $this->getFirstOrderContentTypes();

    $datasourceId = $item->getDatasourceId();
    if ($datasourceId == 'entity:node') {
      $node = $item->getOriginalObject()->getValue();
      $node_type = $node->bundle();
      // Index the bundle label instead of machine name.
      $node_type_label = $node->type->entity->label();

      $fields = $item->getFields();
      $fields = $this->getFieldsHelper()
        ->filterForPropertyPath($fields, NULL, 'content_type_or_other');

      foreach ($fields as $field) {
        $value = 'Other';
        if (!$first_order_types) {
          // If no content types are selected in config form, show all.
          $value = $node_type_label;
        }
        elseif (!empty($first_order_types[$node_type])) {
          $value = $first_order_types[$node_type];
        }

        // Allow other modules to alter the indexed value.
        $event = new SetIndexedValue($node, $value);
        $event_dispatcher = $this->getEventDispatcher();
        $event_dispatcher->dispatch($event, SetIndexedValue::EVENT_NAME);
        $value = $event->getValue();

        $field->addValue($value);
      }
    }

  }

}
