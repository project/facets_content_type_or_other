<?php

namespace Drupal\facets_content_type_or_other\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Defines a form that configures devel settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The node type storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeTypeStorage;

  /**
   * Constructs a new SettingsForm.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->nodeTypeStorage = $entity_type_manager->getStorage('node_type');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'facets_content_type_or_other_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'facets_content_type_or_other.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $options = [];
    foreach ($this->nodeTypeStorage->loadMultiple() as $key => $type) {
      $options[$key] = $type->label();
    }

    $form['first_order_config'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Content type'),
        $this->t('Is first-order?'),
        $this->t('Label override'),
        $this->t('Sort order'),
      ],
      '#attributes' => ['id' => 'first-order-config-table'],
      '#tabledrag' => [
        '#options' => [
          'action' => 'order',
          'group' => 'foct-sort-weight',
          'relationship' => 'sibling',
        ],
      ],
    ];

    $config = $this->config('facets_content_type_or_other.settings')->get('first_order_config');

    $weight = 0;
    $first_order_config_rows = [];
    foreach ($options as $key => $label) {
      $first_order_config_rows[$key] = [
        '#attributes' => [
          'id' => Html::cleanCssIdentifier("foct-{$key}"),
          'class' => ['draggable'],
        ],
        'content_type' => ['#plain_text' => $label],
        'first_order' => [
          '#type' => 'checkbox',
          '#default_value' => !empty($config[$key]['first_order']) ? TRUE : FALSE,
        ],
        'label_override' => [
          '#type' => 'textfield',
          '#default_value' => !empty($config[$key]['label_override']) ? $config[$key]['label_override'] : $label,
        ],
        'weight' => [
          '#type' => 'number',
          '#value' => $config[$key]['weight'] ?? $weight,
          '#attributes' => ['class' => ['foct-sort-weight']],
        ],
        '#weight' => $config[$key]['weight'] ?? $weight,
      ];

      $weight++;
    }

    // Give it another sort so the rows are in the right order.
    uasort($first_order_config_rows, ['Drupal\Component\Utility\SortArray', 'sortByWeightProperty']);
    $form['first_order_config'] = array_merge($form['first_order_config'], $first_order_config_rows);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $first_order_config = $form_state->getValue('first_order_config');

    uasort($first_order_config, ['Drupal\Component\Utility\SortArray', 'sortByWeightElement']);

    $this->config('facets_content_type_or_other.settings')
      ->set('first_order_config', $first_order_config)
      ->save();

    parent::submitForm($form, $form_state);
  }

}
